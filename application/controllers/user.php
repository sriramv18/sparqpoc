<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class user extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
		 $this->load->library('form_validation');
		 
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Siddharth : Dashboard';
        
         $this->load->model('employeedetails_model');
		 
	   
		 $this->load->model('dahsboard_Model');
		 
	    $data['EmpCount'] = $this->employeedetails_model->getEmployeeCount();
		
	    $data['totalpurchaseorder']=$this->dahsboard_Model->totalpurchaseorder(); 
		
		//print_r( $data['totalpurchaseorder']);
	   $data['totalordervalue']=$this->dahsboard_Model->totalordervalue(); 
	   $data['pendingpo']=$this->dahsboard_Model->pendingpo(); 
	   $data['totalserviceamount']=$this->dahsboard_Model->totalserviceamount(); 
	   $data['totalcapitalamount']=$this->dahsboard_Model->totalcapitalamount(); 
	   $data['totalimportamount']=$this->dahsboard_Model->totalimportamount(); 
	   $data['totalrevenueamount']=$this->dahsboard_Model->totalrevenueamount(); 
	   //6th month
	   $data['totalservicepojune']=$this->dahsboard_Model->totalservicepojune(); 
	   $data['totalrevenuepojune']=$this->dahsboard_Model->totalrevenuepojune(); 
	   $data['totalimportpojune']=$this->dahsboard_Model->totalimportpojune(); 
	   $data['totalcapitalpojune']=$this->dahsboard_Model->totalcapitalpojune(); 
	   //7th month
	   $data['totalservicepojuly']=$this->dahsboard_Model->totalservicepojuly();
	   $data['totalrevenuepojuly']=$this->dahsboard_Model->totalrevenuepojuly(); 
	   $data['totalimportpojuly']=$this->dahsboard_Model->totalimportpojuly(); 
	   $data['totalcapitalpojuly']=$this->dahsboard_Model->totalcapitalpojuly();
	   
	    $data['totalservicepo']=$this->dahsboard_Model->totalservicepo();
	    $data['totalrevenuepo']=$this->dahsboard_Model->totalrevenuepo();
		$data['totalimportpo']=$this->dahsboard_Model->totalimportpo();
	    $data['totalcapitalpo']=$this->dahsboard_Model->totalcapitalpo();	   
	   
	   $data['details']=$this->dahsboard_Model->reqdetail();  
	   
       $data['pending_details']=$this->dahsboard_Model->req_pending();
       $data['list']=$this->dahsboard_Model->req_list();
	   
	    $data['serviceprogress']=$this->dahsboard_Model->serviceprogress(); 
		$data['revenueprogress']=$this->dahsboard_Model->revenueprogress(); 
		$data['importprogress']=$this->dahsboard_Model->importprogress(); 
	    $data['capitalprogress']=$this->dahsboard_Model->capitalprogress(); 
		$data['qdetails']=$this->dahsboard_Model->podetails();
        $data['dppo']=$this->dahsboard_Model->dept_po_details();	
		
        
        $this->loadViews("dashboard", $this->global,  $data , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
             $method = $_SERVER['REQUEST_METHOD'];

				if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
				} else {

			$check_auth_client = $this->MyModel->check_auth_client();
	         
			
			if($check_auth_client == true){
				$params = json_decode(file_get_contents('php://input'), TRUE);
				
				$username =   $_GET['username'];
				$password =   $_GET['password'];
			   $this->load->model('login_model');
		        $response=  $this->login_model->loginMe($username,$password);
		  
				json_output($response['status'],$response);
			}
		}
         
            $data['userRecords'] = $this->user_model->userListing();
            
           // $this->global['pageTitle'] = 'Siddharth : User Listing';
            
           // $this->loadViews("users", $this->global, $data, NULL);
       
    }
	

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {            		
			$data['EmpList'] = $this->user_model->getAllEmployees();
            
			$data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Siddharth : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    /* Validation for Employee  Dropdown
	*/
	function  Employee_validate($selectValue)
{
	//echo 'select_validate  method called';
    // 'none' is the first option and the text says something like "-Choose one-"
    if($selectValue == '0')
    {
        $this->form_validation->set_message('Employee_validate', 'Please Select Employee.');
        return false;
    }
    else // user picked something
    {
        return true;
    }
}
    /* Validation for Employee  Dropdown
	*/
	function  Role_validate($selectValue)
{
	//echo 'select_validate  method called';
    // 'none' is the first option and the text says something like "-Choose one-"
    if($selectValue == '0')
    {
        $this->form_validation->set_message('Role_validate', 'Please Select Role.');
        return false;
    }
    else // user picked something
    {
        return true;
    }
}
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
           
             $this->form_validation->set_rules('EmpList','EmpList','trim|callback_Employee_validateS');
			  $this->form_validation->set_rules('role','role','trim|callback_Role_validate');        
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
           
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
				$EmpID = $this->input->post('EmpList');
				$roleId = $this->input->post('role');
				$password = $this->input->post('password');
				 $dt = new DateTime('now', new DateTimeZone('Asia/Kolkata'));				
				$Createddt =  $dt->format('Y-m-d H:i:s');
           
                $email = $this->input->post('MailID');
              
             
				$userInfo = array('email'=>$email,'EmpID'=>$EmpID,'password'=>getHashedPassword($password),'roleId'=>$roleId,'createdBy'=>$this->vendorId,'createdDtm'=>$Createddt);
				
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
					 
					redirect('userListing','refresh');
					echo "<script>alert('New User Created successfully!');</script>";
               
 }
                else
                {
					
                    redirect('userListing','refresh');
		echo "<script>alert('Failiure User Creation!');</script>";			 
                }               
                
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($UserId = '')
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($UserId == '')
            {
               // redirect('userListing');
            }
			
            $Uid =  $_GET['UID'];
			
		
			
            $data['roles'] = $this->user_model->getUserRoles();
			
			//print_r($data);
			
		    $data['EmpList'] = $this->user_model->GetEmployeeDetails($Uid);
			//print_r($data);
			$data['EmpRole'] =   $this->user_model->GetRoleNameByUserID($Uid); 
			 
           // $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'Siddharth : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$userId =   $this->input->post('EmpList'); 
            $this->form_validation->set_rules('EmpList','EmpList','trim|callback_Employee_validateS');
			  $this->form_validation->set_rules('role','role','trim|callback_Role_validate');        
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $EmpID = $this->input->post('EmpList');
				$role = $this->input->post('role');
				$password = $this->input->post('password');
				 $dt = new DateTime('now', new DateTimeZone('Asia/Kolkata'));				
				$updatedDate =  $dt->format('Y-m-d H:i:s');
           
                          //    echo $EmpID;
			
				$userInfo = array();
				
			   
				$userInfo = array('password'=>getHashedPassword($password),'roleId'=>$role,'EmpId'=>$EmpID,'updatedDtm'=>$updatedDate);
				
				
            
                
                $result = $this->user_model->editUser($userInfo,  $EmpID);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
               redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
              echo 'Delete';
			  $Uid = $this->input->post('id');
            
			  $dt = new DateTime('now', new DateTimeZone('Asia/Kolkata'));				
				$updatedDate =  $dt->format('Y-m-d H:i:s');
				
				$userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>$updatedDate);
           
				$result = $this->user_model->deleteUser($Uid, $userInfo);           
            
			echo 'Succssfully change the user status to InAcive';
       
    }
    
    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'Siddharth : Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    
    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:sa'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('loadChangePass');
            }
        }
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Siddharth : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>
