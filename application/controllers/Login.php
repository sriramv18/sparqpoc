<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Login extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {

        parent::__construct();
        $this->load->model('Login_model');
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
		$this->output->set_header('Access-Control-Allow-Origin: *');
		$this->output->set_header('Content-Type: application/json');
		//header('Access-Control-Allow- Methods: POST, GET, PUT, DELETE, OPTIONS'); 
		//header('Access-Control-Allow-Headers: X-Requested-With, content-type, X-Token, x-token');
    }
   public function login()
	{
		
		
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			   
			
				
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$emailID = $params['username'];
		        $password = $params['password'];
				
			   $this->load->model('login_model');
		        $response=  $this->login_model->loginMe($emailID,$password);
							
		  
				//json_output($response['status'],$response);
			if($response['status1']==0){
			json_output(200,$response);
			}
			else {
			json_output(201,$response);
			}
			
			
		}
	}
    


}

?>
