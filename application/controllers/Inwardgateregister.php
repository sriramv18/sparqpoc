<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Class :  Inward Gate Register Controller (security)
 * User Class to control all user related operations.
 * @author : Saravana kumar
 * @version : 1.1
 * @since : 8 Jun 2017
 */
class Inwardgateregister extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
	 public function __construct()
    {
        parent::__construct();
        $this->load->model('Inwardgateregister_model');	   
		header('Access-Control-Allow-Origin: *'); 
		header('Content-Type: application/json');
		$this->output->set_header('Access-Control-Allow-Origin: *');
		$this->output->set_header('Content-Type: application/json');		
      
    }
	
	// To view all the Inward gate register 
	function ViewIGR()
	{		
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {		   
			
				
			//	$params = json_decode(file_get_contents('php://input'), TRUE);
				
			
			   $this->load->model('inwardgateregister_model');
		        $response=  $this->inwardgateregister_model->igrListing();			
			
		  
				json_output($response['status'],$response);
		
		}
	}
	
	//To get PO Number
	 function addinwardgateregister()
    {
		   $method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {		   
			
				
			//	$params = json_decode(file_get_contents('php://input'), TRUE);
				
			
			   $this->load->model('inwardgateregister_model');
		        $response=  $this->inwardgateregister_model->getpurchaseorder();		
			
		  
				json_output($response['status'],$response);
		
		}    
		
		
    }
	
	function getDateformat($Val)
	{
		       $date = new DateTime($Val,new DateTimeZone('Asia/Kolkata'));
			 //  print_r($date);
				$retDate = $date->format('Y-m-d H:i:s');
				return $retDate;
	}

	function IGRITEM()
	{
		
		  $method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$params = json_decode(file_get_contents('php://input'), TRUE);
		$PO = $params['PONO'];
		//$CreatedBy = $this->session->userdata('userId');
		//$this->inwardgateregister_model->UpdatePOMaster($PO,$CreatedBy); 
		$this->load->model('inwardgateregister_model');
		$response = $this->inwardgateregister_model->viewpurchaseorder($PO);
	
		 json_output($response['status'],$response);
	    }
      
		
				
	}
	  function addNewigr()
    {
				
               $method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {		   
					$this->load->model('inwardgateregister_model');
				$params = json_decode(file_get_contents('php://input'), TRUE);
				
			//print_r($params);die;			 $file = $params['igrfile'];			 				if(!empty($file)){								$folderPath = 'uploads/igrfiles/';        $image_parts = explode(";base64,", $file);        $image_type_aux = explode("image/", $image_parts[0]);        $image_type = $image_type_aux[1];        $image_base64 = base64_decode($image_parts[1]);        $file = $folderPath . "img-".date('Y-m-d-').uniqid() . '.png';			$url = base_url();		$document = $url.$folderPath."img-".date('Y-m-d-').uniqid() . '.png';					echo $document;die;        file_put_contents($file, $image_base64);		}die;	//echo $file;		     // $PONO = $params['lineitem'][0]['PONO'];                                                       // $DeliveryChellanOrInvoiceNo = $params['challanNo'];				                				// $DeliveryChellanDate =  $params['invdate'];											// $MaterialRcvdDt = $params['materialdate'];								// if(!empty($params['vechicleNo'])){                // $VehicleNo = $params['vechicleNo'];				// }else{					// $VehicleNo ='';				// }				// if(!empty($params['courierNo'])){                // $CourierNo = $params['courierNo'];				// }else{					// $CourierNo ='';				// }				                // $CreatedBy = $params['CreatedBy'];                // //$Remarks =  $this->input->post('Remarks');				// //$RowCount =  $this->input->post('txtRowCount');				// //echo $RowCount;				 // $dt = new DateTime('now', new DateTimeZone('Asia/Kolkata'));								// $createddt =  $dt->format('Y-m-d H:i:s');								// $IGRStatus = IGR_CREATED;			    // //$document = $file;											// //echo $RowCount;									// $igr = array(); 									// $igr = array('PONO'=>$PONO,'VehicleNo'=>$VehicleNo,'DeliveryChellanOrInvoiceNo'=>$DeliveryChellanOrInvoiceNo,'DeliveryChellanDate'=>$DeliveryChellanDate,'MaterialRcvdDate'=>$MaterialRcvdDt,'CourierNo'=>$CourierNo,'IGRStatus'=>$IGRStatus,'CreatedBy'=>$CreatedBy,'CreatedDate' =>$createddt,'file'=>$document);  				    // //print_r($igr);die();										// $igrM  = $this->inwardgateregister_model->addigrM($igr);					                    // $OGRStatus=IGR_CREATED;					// $check_OGRPO= $this->inwardgateregister_model->update_OGR($PONO,$OGRStatus);					// //print_r($igrM);die;										// $IGRNO = '';					// if(count($igrM)>0)					// {					 // foreach ($igrM as $key ) {					 	// $IGRNO=$key->IGRNO;					 // }					// }					// //echo $IGRNO;						// //$IGRNO = 'IGRNO0692';					// $IGRItemStatus = REQITEM_NEW;										// foreach($params['lineitem'] as $lineitem){												// $MaterialCode = $lineitem['MaterialCode'];						// $QuantityAsPerInvoice = $lineitem['AdvisedQuantity'];						// $OrderedQuantity = $lineitem['Quantity'];						// $ReceivedQty  = $lineitem['ReceivedQuantity'];						// $PendingQty	 = $lineitem['PendingQty'];						// //echo $QuantityAsPerInvoice;																																				// if(strlen($QuantityAsPerInvoice)>0 && $QuantityAsPerInvoice != '0')								// {									// $Remarks = "Igr App Created"; 								    // $ItemStatus = '';									// //if($QuantityAsPerInvoice ==  $OrderedQuantity)																			// if($PendingQty == 0.00 )																   // {										// $ItemStatus  = IGR_CREATED;									// }									// else									// {										// $ItemStatus  = POLINEITEM_IGRPARTIAL_CREATED;									// }																	// $CreatedBy = $params['CreatedBy'];															// $dt = new DateTime('now', new DateTimeZone('Asia/Kolkata'));								// $createddt =  $dt->format('Y-m-d H:i:s');								// $bankstatus =NO_PAIDIGR;										// $igrDetails = array('IGRNO'=>$IGRNO,'MaterialCode'=>$MaterialCode,'QuantityAsPerInvoice'=>$QuantityAsPerInvoice,'Remarks'=>$Remarks,'IGRItemStatus'=>$IGRItemStatus,'CreatedBy'=>$CreatedBy,'CreatedDate'=>$createddt,'BankStatus'=>$bankstatus);										// //print_r($igrDetails);die;										// $igrD = $this->inwardgateregister_model->addigrD($igrDetails);										// //print_r($igrD);die;										// $igrlineno ='IGRItemNo1348';										// // if(!empty($igrD))										// // {											// // foreach($igrD as $ig)											// // {												// // $igrlineno = $ig->IGRItemNo;											// // }										// // }                           // $itemvalue='';                           // $SupplierId='';                           // $polineitemvalue = $this->inwardgateregister_model->getitemvalue($PONO,$MaterialCode);                		    	  // foreach($polineitemvalue as $it)		    	              // {                                 // $itemvalue =$it->Rate;                                 // $SupplierId =$it->SupplierID;		    	               // }										// $historydetails = array('MaterialCode'=>$MaterialCode,'Transaction_type'=>"Add",'Ref_Type'=>"IGR",'Ref_No'=>$igrlineno,'Quantity'=>$QuantityAsPerInvoice,'CreatedBy'=>$CreatedBy,'CreatedOn'=>$createddt,'SupplierID'=>$SupplierId,'ItemValue'=>$itemvalue);										// $this->inwardgateregister_model->addmaterialhistory($historydetails);																				// $Recqty =    $this->inwardgateregister_model->getPOLineItemReceivedQty($PONO,$MaterialCode);										// $ReceivedQuantity = 0.00;											// if(count($Recqty)>0)											// {											 // foreach ($Recqty as $key ) {												// $ReceivedQuantity=$key->ReceivedQuantity;											 // }											// }										 // $totalReceivedqty =  $ReceivedQuantity + $QuantityAsPerInvoice;										 										// //echo '';										 										// $POLineItem = array('ReceivedQuantity'=>$totalReceivedqty,'Status'=>$ItemStatus,'UpdateBY'=>$CreatedBy,'UpdatedOn'=>$createddt );									   									   // $this->inwardgateregister_model->UpdatePOLineItem($POLineItem,$PONO,$MaterialCode);								// }																		 // }					   // $this->inwardgateregister_model->UpdatePOMaster($PONO,$CreatedBy);                					      
					
  			
					 }		$IGRNO = '1';
				$response ['IGR'] = array('status' => 200,'message' => 'IGR Created Successfully '.$IGRNO);
		
			
	json_output($response['IGR']['status'],$response);
	}
	
	
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Siddharth : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>
