<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/**** USER DEFINED CONSTANTS **********/

define('ROLE_ADMIN',                            '1');
define('ROLE_MANAGER',                         	'2');
define('ROLE_EMPLOYEE',                         '3');
/*define('ROLE_PRODUCTION',                       '4');
define('ROLE_QUALITY',                        	'5');
define('ROLE_SECURITY',                         '6');
define('ROLE_SALES',	                        '7');
define('ROLE_ACCOUNTS',                         '8');*/


/***** USER DESIGNATION ******/
//define('SECURITY',                            'SECURITY');

define('SEGMENT',								2);

/**************  Requistion Status code                 */
define('REQ_DRAFT',                            'ST001');
define('REQ_PENDING_APPROVAL',                 'ST002');
define('REQITEM_NEW',                          'ST003');
define('REQ_APPROVED',                         'ST004');
define('REQ_PO_CREATED' ,                       'ST005');
define('REQ_REJECTED',                          'ST006');
define('REQ_CLOSED',                            'ST007');
define('REQ_ONHOLD',                            'ST008');
define('REQITEM_ONHOLD',                        'ST009');
define('REQITEM_PARTIALLY_DELIVERED',           'ST010');
define('REQITEM_DELIVERED',                     'ST011');
define('REQ_DELETED',                     'ST013');
define('REQITEM_POCREATED',                     'ST022');
define('REQITEM_PARTIALLY_PO_CREATED',                     'ST023');
define('REQITEM_Emergency_PO_CREATED',                     'ST024');
define('POLINEITEM_IGR_CREATED',                     'ST040');
define('REQ_READYFORDELIVERY',                     'ST049');
define('REQ_DELIVERED',                     'ST050');
/*IGR Status */
define('IGR_CREATED',                     'ST027');
define('IGR_CLOSED',                     'ST028');
define('IGR_CANCELLED',                     'ST029');
define('POLINEITEM_IGRPARTIAL_CREATED',                     'ST042');
/*IGR Status*/

/*MRIR Status */
define('MRIR_CREATED',                     'ST041');
define('MRIR_APPROVED',                     'ST044');
define('MRIR_REJECTED',                     'ST045');
define('PARTIAL_MRIRAPPROVED',               'ST054');

define('REQUISTSTATUS',                     '0');
define('REQUISTITEMSTATUS',                     '1');

/* Purchase Order Status */

define('PO_DRAFT',                            'ST014');
define('PO_CREATED',                          'ST015');
define('PO_INPROGRESS',                       'ST016');
define('PO_DISPATCHED',                       'ST017');
define('PO_DELIVERED',                        'ST018');
define('PO_CANCELLED',                        'ST019');
define('PO_ONHOLD',                           'ST020');
define('PO_CLOSED',                           'ST021');
define('PO_AMENDED',                     'ST030');
define('PO_RELEASED',                     'ST026');
define('PO_AWAITING_RELEASE',                     'ST025');
define('PO_APPROVED',                     'ST025');
define('PO_APPROVER_ONHOLD',                     'ST051');
define('PO_RELEASER_ONHOLD',                     'ST052');
define('SPECIAL_PO',                           'ST056');

/*store Status*/

define('STORE_DRAFT',                            'ST043');
define('STORE_CREATED',                            'ST015');

/*store requisition*/
define('STORE REQUISTION STATUS',                     '6');
define('STORE_OPEN',                            'ST034');
define('STORE_PENDINGAPPROVAL',                 'ST035');
define('STORE_APPROVAL',                        'ST036');
define('STORE_ISSUSED',                         'ST037');
define('STORE_PARTIALLYISSUSED',                'ST055');
define('STORE_ONHOLD',                          'ST038');
define('STORE_REJECT',                          'ST039');

/* OGR status */
define('OGR_PARTIAL_COMPLETE',                  'ST064');
define('OGR_COMPLETE',                  		'ST063');
define('ST044',									'ST044');
define('PO',									'PO');
define('ST063',									'ST063');
define('OGR_CREATED',						    'ST065');
define('OGR_NOT_CREATED', 						'ST069');




//pomaster status update//
define('AMOUNT_PAID',                            'ST057');
define('NO_PAID',                                'ST058');
define('PARTIALLY_PAID',                         'ST059');
define('AMOUNT_RECEIVED',                            'ST065');
define('NO_RECEIVED',                                'ST066');
define('PARTIALLY_RECEIVED',                         'ST067');
define('AMOUNT_PAIDIGR',                         'ST070');
define('PARTIALLY_PAIDIGR',                      'ST071');
define('NO_PAIDIGR',                             'ST072');
/* Work status code */

define('SERVICE_COMPLETED',                          'ST046');
define('PO_SERVICE_COMPLETED',                          'ST053');

/* Department Code */
define('ADMIN',                            			'DEP01');
define('QUALITY',                            		'DEP02');
define('STORE',                            			'DEP03');
define('SALES',                            			'DEP04');
define('PRODUCTION',                            	'DEP05');
define('DELIVERY',                            		'DEP06');
define('TRP',                            			'DEP07');
define('COATING',                            		'DEP08');
define('MAINTENANANCE',                            	'DEP09');
define('FINANCE',                            		'DEP10');
define('PURCHASE',                            		'DEP11');
define('MANAGEMENT',                            		'DEP12');
define('HR',                            		'DEP13');
define('SECURITY',                            		'DEP15');
/* PO Type  */
define('REVENUE',                            		'REVENUE');
define('SERVICE',                            		'SERVICE');
define('IMPORT',                            		'IMPORT');
define('CAPITAL',                            		'CAPITAL');
define('SYSTEM_ADMIN',                            	'1');
// Tax Type
define('PERCENTAGE',                            		'PERCENTAGE');
define('LUMPSUM',                            		'LUMP SUM');
define('NILTYPE',                            		'NIL');

// Freight Type
define('PERUOM',                            		'PER UOM');
define('PERTRIP',                            		'PER TRIP');

//


/*  Formula for Tax calculation */
/* Discount*/
define('DIS_PERCENTAGE',                            		'((Discountval/100) * basicValue)');
define('DIS_LUMPSUM',                            		    'Discountval');
// Packaging
define('PACKAGING_PERCENTAGE_BYDISCOUNTVALUE',             		'((Packagingvalue/100) * (basicValue-AfterDiscount))');
define('PACKAGING_PERCENTAGE_BYBASICVALUE',                            		'(Packagingvalue/100) * basicValue');
define('PACKAGING_LUMPSUM_BYDISCOUNTVALUE',             		'Packagingvalue');
define('PACKAGING_LUMPSUM_BYBASICVALUE',                            		'Packagingvalue');
//Excise
define('EXCISE_ONBASICVALUE',                            		'((ExciseDuty/100) * basicValue)');
define('EXCISE_ONBASIC_DISCOUNT',                            		'((ExciseDuty/100) * (basicValue-AfterDiscount))');
define('EXCISE_ONBASIC_DISCOUNT_PACKAGING',                      '((ExciseDuty/100) * ((basicValue-AfterDiscount)+ AfterPackaging))');
define('EXCISE_ONBASIC_PACKAGING',                            		'((ExciseDuty/100) * (basicValue + AfterPackaging))');


//Vat
define('VAT_ONBASICVALUE',                            		'((vatValue/100) * basicValue)');
//define('VAT_ONEXICSEVALUE',                            		'((vatValue/100) * (basicValue+ExciseDuty))');
define('VAT_ONEXICSEVALUE',                            		'(((basicValue - discountValue) + (packageValue + ExciseDuty)) * (vatValue/100))');

//CST
define('CST_ONBASICVALUE',                            		'((CSTValue/100) * basicValue)');
//define('CST_ONEXICSEVALUE',                            		'((CSTValue/100) * (basicValue+ExciseDuty))');
define('CST_ONEXICSEVALUE',                            		'(((basicValue - discountValue) + (packageValue + ExciseDuty)) * (CSTValue/100))');
//GST
define('GSTTAX',                            				'((GSTValue/100) * basicValue)');
//Other Tax
define('OTHERTAX',                            				'((OtherTaxValue/100) * basicValue)');
//Freight Tax
define('FREIGHT_PERCENTAGE',                            			'((FreightValue/100) * basicValue)');
define('FREIGHT_PERCENTAGE_AFTER',                                  '((FreightValue/100) * (basicValue-AfterDiscount))');
define('FREIGHT_LUMPSUM',                            				'FreightValue');
define('FREIGHT_PERUOM',                            				'FreightValue * Quantity');
define('FREIGHT_PERTRIP',                            				'FreightValue');

/*           SERVICE PO VARIABLES    */
define('SERVICETAX_SERVICETAX_14PERCENT',                            	'((14/100) * basicValue)');
define('SERVICETAX_EDUCESS_2PERCENT',                            		'((2/100) * ServiceTaxAmount)');
define('SERVICETAX_HIGHEREDUCESS_1PERCENT',                            	'((1/100) * ServiceTaxAmount)');
define('SERVICETAX_KRISHIKALYAN',                            			'(((.5)/100) * basicValue)');
define('SERVICETAX_SWACHHBHARAT',                            			'(((.5)/100) * basicValue)');

/*IMPORT PO CALCULATIONS*/


define('LOADING_CHARGE',                                            '(txtlanding*txtbasicprice/100)');
define('HIGHSEAS_SALES',                                  '(totalhighsess*highsesspercentage/100)');
define('CUSTOM_DUTY',                                     '(sumcustom*txtcustompercentage)/100');
define('EXCISE_DUTY',                                      '(txtexcisesum*txtpercentage)/100');
define('EXCISE_ED',                                        '(txtexcise*txtexcisepercentage)/100');
define('EXCISE_SH',                                        '(excisetxt*exciseshcess)/100');
define('CUSTOM_ED',                                        '(totalval*txtedcesspercentage)/100');
define('CUSTOM_SH',                                        '(totcustomsh*txtshpercentage)/100');
define('ADDTNL_EXCISEDUTY',                                '(totaddtnlexcise*addtnpercentage)/100');
define('GROSS_EXP',                                        '(txtgrossdutypayable-txtmodvat)');
define('AVL_MODVAT',                'txtexciseduty1+txtexciseedcess+txtexciseshcess+txtaddtnlexcise');
define('GROSS_DUTY',   'landingcha+highsesssalless+customcha+excisecha+exciseedcha+exciseshcha+customed+customsh+txtaddtnexciseduty');
define('PURCHASE_RATE',                                    'txtbasicinrprice/quantity');
define('CUSTOMDUTY_EXP',                                   'txtgrossexp/quantity');
define('RMC_CAL',                                          'txtpurchaserate+txtcustomdutyexp');
define('CALCULATE_TOTAL',                                  '(txtrmc*quantity)+basicprice');
/* Capital Po Calculations */
define('Capital_Landing_Charge',                                            'basicPrice*(landCharge/100)');
define('Capital_Highseas_Charge',                                            '((highSeas/100) * (basicPrice+afterLandingCharge))');
define('Capital_Customduty_Charge',                                            '((Customduty/100) * (basicPrice+afterLandingCharge+afterHighseas))');
define('Capital_Excise_Charge',                                            '((Excise/100) * (basicPrice+afterLandingCharge+afterHighseas+afterCustomduty))');
define('Capital_ExciseEDcess_Charge',                                            '(ExciseEDcessCharge/100) * ExciseDuty');
define('Capital_ExciseSHcess_Charge',                                            '(ExciseSHcessCharge/100) * ExciseDuty');
define('Capital_CustomEDcess_Charge',                                            '((CustomEDcess/100) * (afterLandingCharge+AfterHighseas+AfterCustomduty+AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess))');
define('Capital_CustomSHcess_Charge',                                            '((CustomSHcess/100) * (afterLandingCharge+AfterHighseas+AfterCustomduty+AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess))');
define('Capital_AdditionalExciseduty_Charge',                                            '((AdditionalExciseduty/100) * (basicPrices+afterLandingCharge+AfterHighseas+AfterCustomduty+AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess+AfterCustomEDcess+AfterCustomSHcess))');

define('Capital_Grossdutypayable_Charge',                                            '(AfterAdditionalExciseduty+afterLandingCharge+AfterHighseas+AfterCustomduty+AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess+AfterCustomEDcess+AfterCustomSHcess)');
define('Capital_ModvatCharge_Charge',                                            '(AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess+AfterAdditionalExciseduty)');

define('Capital_Grossexpenses_Charge',                                            '(Grossdutypayable-AvailableModvat)');
define('Capital_purchaserate_Charge',                                            '(AfterBasicPriceTax/PurQuantity)');
define('Capital_CustomDutyExpenses_Charge',                                            '(Grossexpenses/PurQuantity)');
define('Capital_RMCIncludingCustomers_Charge',                                            '(purchaserate+CustomDutyExpenses)');
/* calculate cgst sgst igst calculations for service tax*/
define('SERVICETAX_CGST',                                            '((Cgst/100) * basicValue)');
define('SERVICETAX_SGST',                                            '((Sgst/100) * basicValue)');

define('SERVICETAX_IGST',                                            '((Igst/100) * basicValue)');





/*New import Calculation*/
define('LOADING_CHARGE1',                                           '(txtlanding*txtbasicprice/100)');
define('HIGHSEAS_SALES1',                                  '(totalhighsess*highsesspercentage/100)');
define('CUSTOM_DUTY1',                                     '(accessible*txtcustompercentage)/100');
define('EXCISE_DUTY1',                                      '(txtexcisesum*txtpercentage)/100');
define('EXCISE_ED1',                                        '(txtexcise*txtexcisepercentage)/100');
define('EXCISE_SH1',                                        '(excisetxt*exciseshcess)/100');
define('CUSTOM_ED1',                                      '(txtcustomduty1*txtedcesspercentage)/100');
define('CUSTOM_SH1',                                        '(customch*txtshpercentage)/100');
define('ADDTNL_EXCISEDUTY1',                                '(totaddtnlexcise*addtnpercentage)/100');
define('GROSS_EXP1',                                        '(txtgrossdutypayable-txtmodvat)');
define('AVL_MODVAT1',               'txtexciseduty1+txtexciseedcess+txtexciseshcess+txtaddtnlexcise');
define('GROSS_DUTY1',                                       'custom+customsh+customed+afterigst');
define('PURCHASE_RATE1',                                    '(dutyimpact)/quantity');
define('CUSTOMDUTY_EXP1',                                   'txtgrossexp/quantity');
define('RMC_CAL1',                                          'txtpurchaserate+txtcustomdutyexp');
define('CALCULATE_TOTAL1',                                  '(txtrmc*quantity)+basicprice');

define('IGST',                                      '(tot*igstpercentage)/100');
define('SUBTOTAL',                                  '((assessble)+(custom)+(customsh)+(customed))');

define('ASSESSBLE',                                  '(result+txtbasicprice)');
define('DUTYIMPACT',                                  '(totduty-igst)');
define('MATBYQTY',                                  '(basicqtyval)/qty');
define('CLEARINGQTY',                                  '(clearing/qty)');
define('NETTPERUOM',               				'(basicres+clear+customdutyexp)');


/*Capital Calculation*/

define('Capital_Landing_Charge1',                                            'basicPrice*(landCharge/100)');

define('Capital_Highseas_Charge1',                                            '((highSeas/100) * (basicPrice+afterLandingCharge))');

define('Capital_Customduty_Charge1',                                '((Customduty/100) *accessible)');

define('Capital_Excise_Charge1',                                            '((Excise/100) * (basicPrice+afterLandingCharge+afterHighseas+afterCustomduty))');

define('Capital_ExciseEDcess_Charge1',                                            '(ExciseEDcessCharge/100) * ExciseDuty');

define('Capital_ExciseSHcess_Charge1',                                            '(ExciseSHcessCharge/100) * ExciseDuty');

define('Capital_CustomEDcess_Charge1',                   '((CustomEDcess* AfterCustomduty)/100)');



define('Capital_CustomSHcess_Charge1',                  '((CustomSHcess/100) * AfterCustomduty)');

define('Capital_AdditionalExciseduty_Charge1',                                            '((AdditionalExciseduty/100) * (basicPrices+afterLandingCharge+AfterHighseas+AfterCustomduty+AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess+AfterCustomEDcess+AfterCustomSHcess))');


define('Capital_Grossdutypayable_Charge1',                                            '(AfterAdditionalExciseduty+afterLandingCharge+AfterHighseas+AfterCustomduty+AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess+AfterCustomEDcess+AfterCustomSHcess)');

define('Capital_ModvatCharge_Charge1',                                            '(AfterExcisedutyTax+AfterExcisedutyEDcess+AfterExcisedutySHcess+AfterAdditionalExciseduty)');


define('Capital_Grossexpenses_Charge1',                                            '(Grossdutypayable-AvailableModvat)');
define('Capital_purchaserate_Charge1',                                            '(AfterBasicPriceTax/PurQuantity)');
define('Capital_CustomDutyExpenses_Charge1',                                            '(Grossexpenses/PurQuantity)');
define('Capital_RMCIncludingCustomers_Charge1',                                            '(purchaserate+CustomDutyExpenses)');



//stock final product

define('Consumables',                            		'Consumables');
define('RawMaterial',                            		'Raw Material');
define('Packingmaterial',                            		'Packing material');
define('FinalProduct',                            		'Final Product');



//Adavnce Payments and Receipts
define('Apayment',                            		'0009');
define('AReceipt',                            		'0013');

/* End of file constants.php */
/* Location: ./application/config/constants.php */