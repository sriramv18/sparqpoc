<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class inwardgateregister_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
	
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function igrListing()
    {	
  		
		$this->db->select('igr.*,PO.DeliveryDate,sup.SupplierName');
		$this->db->from('T_IGR_Master igr');
		$this->db->join('T_PurchaseOrder_Master PO','igr.PONO = PO.PONO');
 		$this->db->join('T_SupplierDetailsN` sup','PO.SupplierID = sup.SupplierID');		
         $this->db->order_by('igr.CreatedDate','desc');  
        $query = $this->db->get();       
        $result = $query->result();  
        
        if(!empty($result)){
           
                return array('status' => 200,'message' => 'All IGR Details.','IGRData' => $result);
         
        } else {
           return array('status' => 200,'message' => 'No Records found.');
               
        }
    }
	
	function getpurchaseorder()
    {

		$this->db->distinct();	   $this->db->select('POM.PONO,POM.ServiceDescription,POM.DeliveryDate,sup.SupplierName,sup.Address,POM.status,OGR_Status');       $this->db->from('T_PurchaseOrder_Master POM');	    $this->db->join('T_PurchaseOrder_LineItem POL', 'POL.PONO = POM.PONO');	   $this->db->join('T_SupplierDetailsN sup', 'POM.SupplierID = sup.SupplierID');	   $this->db->join('T_OGR_Master OGM ', 'POM.PONO = OGM.PONO_INV','left');		$this->db->where('POM.Status !=',PO_AMENDED);	    $this->db->where('POM.Status !=',IGR_CREATED);		$this->db->where('POM.Status',PO_RELEASED);		$this->db->where('POM.POType !=',SERVICE);		 $this->db->or_where('POM.Status',MRIR_CREATED);	   	   $this->db->or_where('POM.Status',MRIR_APPROVED);	   	   $this->db->or_where('POM.Status',OGR_COMPLETE);
		$query = $this->db->get();		
	    
	    $result = $query->result(); 
		//print_r($this->db->last_query());		//print_r($result);die;
		 if(!empty($result)){
           
                return array('status' => 200,'message' => 'All PO List.','IGRPOData' => $result);
         
        } else {
           return array('status' => 200,'message' => 'No Records found.');
               
        }
    }
	
	
	function viewpurchaseorder($PO)
    {
	
		
		$subQuery ='SELECT distinct POM.PONO,POL.MaterialCode,POL.Quantity,POL.ReceivedQuantity,(POL.Quantity -POL.ReceivedQuantity ) as PendingQty,(POL.Quantity -POL.ReceivedQuantity ) as AdvisedQuantity,POL.PONO,MM.MaterialName,MM.UOM
				from T_PurchaseOrder_Master POM   
				 left join  T_PurchaseOrder_LineItem POL on POL.PONO = POM.PONO 
				 left join  T_IGR_Master IGM on IGM.PONO =POL.PONO 
				 left join T_IGR_Details igr on  igr.MaterialCode = POL.MaterialCode and  IGM.PONO =POL.PONO 
				left join T_MaterialMaster MM
				on  MM.MaterialCode = POL.MaterialCode where POM.PONO=? ';
						
			 $query = $this->db->query($subQuery,$PO);	
		 
		  
			 
	    $result = $query->result(); 
		//print_r($this->db->last_query());
		 if(!empty($result)){
           
                return array('status' => 200,'message' => 'All PO Item List.','POItemData' => $result);
         
        } else {
           return array('status' => 200,'message' => 'No Records found.');
               
        }
		
	
    }
	function addigrM($igrM)
    {
	
            $this->db->trans_start();
			$this->db->insert('T_IGR_Master', $igrM);        
			$insert_id = $this->db->affected_rows();         
			$this->db->trans_complete();
			// print_r($this->db->last_query());		
		//print_r($insert_id);
	if($insert_id>0)
			{
				$subQuery = 'select max(IGRNO) as IGRNO from T_IGR_Master';   
          
				$query = $this->db->query($subQuery);
        
				return $query->result();
			}
    }function update_OGR($PONO,$status)	{      $this->db->select('*');      $this->db->from('T_OGR_Master');      $this->db->where('PONO_INV',$PONO);      $query= $this->db->get();      if($query->num_rows!=0)      {      	$this->db->where('PONO_INV',$PONO);      	$ogrstatus=array('Status'=>$status);      	$this->db->update('T_OGR_Master',$ogrstatus);      }	}
		
	function addigrD($igrD)
	{
	
			$this->db->trans_start();
			$this->db->insert('T_IGR_Details', $igrD);        
			$insert_id = $this->db->affected_rows();         
			$this->db->trans_complete();
			
			if($insert_id>0)
			{
				$subQuery = 'select max(IGRItemNo) as IGRItemNo from T_IGR_Details';   
          
				$query = $this->db->query($subQuery);
        
				return $query->result();
			}
	}	function getitemvalue($pono,$MaterialCode)	{      $this->db->select('Rate,SupplierID');      $this->db->from('T_PurchaseOrder_LineItem');      $this->db->join('T_PurchaseOrder_Master','T_PurchaseOrder_Master.PONO=T_PurchaseOrder_LineItem.PONO');      $this->db->where('T_PurchaseOrder_LineItem.PONO',$pono);      $this->db->where('MaterialCode',$MaterialCode);      $query = $this->db->get();      $result = $query->result();              return $result;	}		function addmaterialhistory($historydetails)	{			$this->db->trans_start();			$this->db->insert('T_Material_stock_history', $historydetails);        			$insert_id = $this->db->affected_rows();         			$this->db->trans_complete();			return true;	}		function getPOLineItemReceivedQty($PONO,$MaterialCode)	{		 $this->db->select('ReceivedQuantity');        $this->db->from('T_PurchaseOrder_LineItem');		$this->db->where('PONO',$PONO);		$this->db->where('MaterialCode',$MaterialCode);		 $query = $this->db->get();        // print_r( $this->db->last_query());        $result = $query->result();                return $result;	} 						
	 function viewigr($IGRNO)
    {
		$this->db->distinct();
        $this->db->select('igr.IGRNO,igr.*,igrd.*,MM.MaterialName,MM.UOM,sup.Address,sup.SupplierName,POM.ServiceDescription,POL.Quantity');
        $this->db->select('igr.IGRNO,igr.*,igrd.*,MM.MaterialName,MM.UOM,sup.Address,sup.SupplierName,POM.ServiceDescription,POL.Quantity');
        $this->db->from('T_IGR_Master  igr');
		$this->db->join('T_IGR_Details igrd','igrd.IGRNO = igr.IGRNO');
		$this->db->join('T_PurchaseOrder_Master POM','igr.PONO = POM.PONO');
		$this->db->join('T_PurchaseOrder_LineItem POL','igr.PONO = POL.PONO and POL.MaterialCode=igrd.MaterialCode ');		
		$this->db->join('T_SupplierDetailsN sup', 'sup.SupplierID = POM.SupplierID');		
	    $this->db->join('T_MaterialMaster MM', 'MM.MaterialCode = igrd.MaterialCode');
		$this->db->where('igr.IGRNO',$IGRNO);
		 
        
        $query = $this->db->get();
        // print_r( $this->db->last_query());
        $result = $query->result();        
        return $result;
    }
	
	function UpdatePOLineItem($LineItem,$PONO,$MaterialCode)
	{
		$this->db->where('PONO', $PONO);
		$this->db->where('MaterialCode', $MaterialCode);
        $this->db->update('T_PurchaseOrder_LineItem', $LineItem);
        
        return TRUE;
	}
	
	function UpdatePOMaster($PONO,$updatedBy)
	{
		 $this->db->select('*');
        $this->db->from('T_PurchaseOrder_LineItem');
		$this->db->where('PONO',$PONO);
		$this->db->where('Status !=',IGR_CREATED);
	    
		$query = $this->db->get();
		
		
        if ($query->num_rows == 0) {
			$this->db->where('PONO', $PONO);
			$POStatus = array('status'=>IGR_CREATED,'UpdateBY'=>$updatedBy);
		    $this->db->update('T_PurchaseOrder_Master', $POStatus);
	 return array('status' => 200,'message' => 'IGR Created Successfully');
		}   
		
	}
		
}
