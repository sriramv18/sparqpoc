<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing()
    {
        $this->db->select('EMP.EmpID,EMP.FirstName,EMP.LastName,EMP.Designation,EMP.EmailId,EMP.ContactNumber,
						DEPT.DEPCode,DEPT.DepartmentName,role.role,user.userId');
        $this->db->from('T_Employee_Details EMP');
        $this->db->join('T_DepartmentDetails DEPT', 'EMP.Departmentcode = DEPT.DEPCode');
		$this->db->join('tbl_users user', 'user.EmpID =  EMP.EmpID');
		$this->db->join('tbl_roles role', 'user.roleId = role.roleId');
        
      
        $this->db->where('user.isDeleted !=', 1);
       
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
    function GetRoleNameByUserID($UserId)
	{
		 $subQuery = 'select roleId, role from tbl_roles 
					 where roleId = (select roleId from tbl_users where  userId=?)';
					
		 $query = $this->db->query($subQuery, array($UserId));	
		 
	
		 return $query->result();
        
	}
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
		  $this->db->where("roleid <", 4);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_users', $userInfo);
        
        $insert_id = $this->db->affected_rows();   
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('userId, name, email, mobile, roleId,isDeleted');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
		$this->db->where('roleId !=', 1);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('EmpID', $userId);
        $this->db->update('tbl_users', $userInfo);
        //print_r($this->db->last_query());
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }
	/**
     * This function is used to get Employee details based on Employee ID Selection
     * @return array $result : This is result of the query
     */
	function GetEmployeeDetails($UserId)
    {
		//echo 	$UserId;	 
		 $subQuery = 'select   EMP.EmpID,EMP.FirstName,EMP.LastName,EMP.Designation,EMP.EmailId,EMP.ContactNumber,DEPT.DEPCode,DEPT.DepartmentName from  
						T_Employee_Details EMP join T_DepartmentDetails DEPT on
						EMP.Departmentcode = DEPT.DEPCode  where  EMP.EmpID= (select EmpID from tbl_users where userId=?)';   
          
         $query = $this->db->query($subQuery, array($UserId));	
		 
		
        
          return $query->result();
    }
	/**
     * This function is used to get the All employees
     * @return array $result : This is result of the query
     */
	function getAllEmployees()
    {
		/*$this->db->select('EmpID,FirstName,LastName');
        $this->db->from('T_Employee_Details');
		$query = $this->db->get();
        
        return $query->result(); */
		 $this->db->select('EMP.EmpID,EMP.FirstName,EMP.LastName,EMP.Designation,EMP.EmailId,EMP.ContactNumber,DEPT.DEPCode,DEPT.DepartmentName');
         $this->db->from('T_Employee_Details EMP');
		 $this->db->join('T_DepartmentDetails DEPT', 'EMP.Departmentcode = DEPT.DEPCode');
		  $this->db->where('EMP.IsActive ',1 );
		 $query = $this->db->get();
		 return $query->result();
    }
}

  