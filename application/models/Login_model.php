<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
		 $this->db->select('BaseTbl.userId, BaseTbl.password, Emp.EmpID,Emp.FirstName,Emp.Designation, BaseTbl.roleId, Roles.role,DEPT.DEPCode,DEPT.DepartmentName,DEPT.HeadDept');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Roles','Roles.roleId = BaseTbl.roleId');
		 $this->db->join('T_Employee_Details as Emp','BaseTbl.EmpID = Emp.EmpID');
		  $this->db->join('T_DepartmentDetails as DEPT','Emp.Departmentcode = DEPT.DEPCode');
        $this->db->where('BaseTbl.email', $email);
        $this->db->where('BaseTbl.isDeleted', 0);
           $query = $this->db->get();
        
       
		$user = $query->result();
		
  
        if(!empty($user)){

            if($this->verifyHashedPassword($password, $user[0]->password)){
                return array('status1' => 0,'message' => 'Successfully login.','LoginDetails' => $user);
            } else{				return array('status1'=>1,'message'=>'Password Mismatch');			}
        } else {

          return array('status1' => 1,'message' => 'User Name Wrong.');
               
        }
		
    } 
	
	 function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

?>
